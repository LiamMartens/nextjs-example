const webpack = require('webpack');
const axios = require('axios');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { parsed: localEnv } = require('dotenv').config();

module.exports = {
    pageExtensions: ['tsx', 'ts', 'jsx', 'js'],
    exportPathMap: async function(defaultPathMap) {
        const response = await axios.get(`http://www.omdbapi.com/?apikey=${process.env.OMDB_KEY}&s=Marvel`);
        const customPages = {};
        response.data.Search.forEach(movie => {
            customPages[`/movie/${movie.imdbID}`] = {
                page: '/movie',
                query: { movie: movie.imdbID },
            }
        });

        return Object.assign({}, defaultPathMap, customPages);
    },
    webpack(config, options) {
        config.resolve.extensions.push('ts', 'tsx');
        config.module.rules.push({
            test: /\.(tsx?)$/,
            exclude: /node_modules/,
            use: {
                loader: 'ts-loader',
            }
        })
        config.plugins.push(new webpack.EnvironmentPlugin(localEnv));

        if (options.isServer) {
            config.plugins.push(new ForkTsCheckerWebpackPlugin({
                useTypescriptIncrementalApi: true,
                async: false,
            }));
        }
        return config;
    }
};
