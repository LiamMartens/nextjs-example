import * as React from 'react';
import Link from 'next/link';
import axios from 'axios';

interface IMovie {
    imdbID: string;
    Title: string;
    Year: number;
}

interface IProps {
    movies: IMovie[];
}

class HomePage extends React.Component<IProps> {
    static async getInitialProps({ req }) {
        const response = await axios.get(`http://www.omdbapi.com/?apikey=${process.env.OMDB_KEY}&s=Marvel`);

        return {
            movies: response.data.Search,
        };
    }

    public render() {
        const { movies } = this.props;

        return (
            <>
                <h1>Movies fetched from OMDB</h1>
                <ul>
                    {movies.map(m => (
                        <li key={m.imdbID}>
                            <Link href={`/movie/${m.imdbID}`}>
                                <a>{m.Title}: {m.Year}</a>
                            </Link>
                        </li>
                    ))}
                </ul>
            </>
        );
    }
}

export default HomePage;