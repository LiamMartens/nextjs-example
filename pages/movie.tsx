import * as React from 'react';
import axios from 'axios';
import { withRouter, SingletonRouter } from 'next/router';

interface IMovie {
    imdbID: string;
    Title: string;
    Year: number;
    Plot: string;
}

interface IProps {
    movie: IMovie;
    router: SingletonRouter;
}

interface IState {
    plotVisible: boolean;
}

class MoviePage extends React.Component<IProps, IState> {
    static async getInitialProps({ req, query }) {
        const movie =  typeof query.movie === 'object'
            ? query.movie
            : (await axios.get(`http://www.omdbapi.com/?apikey=${process.env.OMDB_KEY}&i=${query.movie}`)).data;

        return {
            movie,
        };
    }

    public state = {
        plotVisible: false,
    };

    private handleShowPlot = () => {
        this.setState({
            plotVisible: true,
        });
    }

    public render() {
        const { movie } = this.props;
        const { plotVisible } = this.state;

        return (
            <>
                <h1>{movie.Title} / {movie.Year}</h1>
                {plotVisible ? (
                    <p>{movie.Plot}</p>
                ) : (
                    <button onClick={this.handleShowPlot}>Show plot</button>
                )}
            </>
        );
    }
}

export default withRouter(MoviePage);